package com.googlemail.australianfan.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.googlemail.australianfan.control.DatabaseHelper;
import com.googlemail.australianfan.control.TheamChooser;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Highscore extends Activity {

    private ListView listView;
    private ArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TheamChooser.onActivityCreateSetTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);
        listView = (ListView) findViewById(R.id.lVHighscore);
        adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_list_item_1,
                new ArrayList<com.googlemail.australianfan.model.Highscore>());
        listView.setAdapter(adapter);

        try{
            List<com.googlemail.australianfan.model.Highscore> highscoreList =
                    DatabaseHelper.getInstance().getHighscorDao().queryBuilder()
                    .orderBy(com.googlemail.australianfan.model.Highscore.POINTS, true).query();
            if(!highscoreList.isEmpty()){
                adapter.addAll(highscoreList);
                adapter.notifyDataSetChanged();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_highscore, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
