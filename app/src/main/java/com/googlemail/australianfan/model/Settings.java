package com.googlemail.australianfan.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by xtntx on 06.07.15.
 */
@DatabaseTable(tableName = "SETTINGS")
public class Settings {
    public String getName() {
        return name;
    }
    public static final String NAME = "NAME";
    public static final String COLOR = "COLOR";

    @DatabaseField(columnName = "ID", dataType = DataType.INTEGER, generatedId = true)
    private int id;


    @DatabaseField(columnName = NAME, dataType = DataType.STRING)
    private String name;

    @DatabaseField(columnName = COLOR, dataType = DataType.ENUM_INTEGER)
    private ThemColor color;

    public void setName(String name) {
        this.name = name;
    }

    public ThemColor getColor() {
        return color;
    }

    public void setColor(ThemColor color) {
        this.color = color;
    }

    public Integer getId() {
        return id;
    }
}
