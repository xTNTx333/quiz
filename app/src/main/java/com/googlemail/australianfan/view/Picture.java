package com.googlemail.australianfan.view;


import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Picture extends Fragment {
    private static final String ARG_IMAGE = "image";
    private Bitmap image;
    private ImageView imageView;
    private View view;

    public Picture() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Setzt das Bild
     * @param image das Bild
     */
    public void setImage(Bitmap image){
        this.image = image;
        imageView.setImageBitmap(image);
        view.invalidate();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_picture, container, false);
        imageView = (ImageView) view.findViewById(R.id.imageViewQuestion);
        return view;
    }


}
