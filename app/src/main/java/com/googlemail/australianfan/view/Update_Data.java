package com.googlemail.australianfan.view;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.googlemail.australianfan.control.DownloadNewData;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Update_Data.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Update_Data#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Update_Data extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Die ListView auf den Fragment
     */
    private ListView listview;

    /**
     * Die Klasse die die Internet Datenhaltung verwaltet
     */
    private DownloadNewData data = new DownloadNewData();

    /**
     * Der Adapter für die Liste
     */
    private ArrayAdapter adapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Update_Data.
     */
    public static Update_Data newInstance(String param1, String param2) {
        Update_Data fragment = new Update_Data();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Update_Data() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_update__data, container, false);
        android.widget.Button button = (Button)view.findViewById(R.id.aktualisieren);
        // Listener fuer den aktualiesieren button.
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                try {
                    updateListData(view);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        //Liste mit Adapter versehen damit werte Angezeigt werden können
        listview = (ListView) view.findViewById(R.id.packages);
        adapter = new ArrayAdapter(view.getContext(),
                android.R.layout.simple_list_item_1, new ArrayList<String>());
        listview.setAdapter(adapter);
        //Listener hinzugefügt der das Clicken auf ein Item realisiert und ein Warteanimation startet und
        //Das Paket runterlädt (Internetzugriffe müssen asyncron ablaufen)
        listview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,final View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                final ProgressDialog dialog = ProgressDialog.show(view.getContext(),
                        "Update", "Packet " + item + " wird vom Server Geladen", true, false);
                new AsyncTask<String, Void, String>() {

                    @Override
                    protected String doInBackground(String... values) {
                        if(values != null && values.length > 0) {
                            String value = values[0];
                            if (data.downloadPackage(value)) {
                                return value;
                            } else {
                                return null;
                            }
                        }else{
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute( String result) {
                        dialog.cancel();
                        if(result != null){
                            Update_Data.this.removeUpdateList(result);
                        }
                    }
                }.execute(item);
            }
        });
        return view;
    }

    /**
     * Löscht die Liste und setzt die Übergebenen werte.
     * @param values Werte die Angezeigt werden sollen
     */
    private void updateList(List<String> values){
        adapter.clear();
        adapter.addAll(values);
        adapter.notifyDataSetChanged();
    }

    /**
     * Löscht aus der Liste das Übergebene Item
     * @param value Das zu Löschende item
     */
    private void removeUpdateList(String value){
        adapter.remove(value);
        adapter.notifyDataSetChanged();
    }

    /**
     * Methode zum updaten der Liste vom Server
     * @param view die View vom Aufruf
     */
    private void updateListData(final View view){
        final ProgressDialog dialog = ProgressDialog.show(view.getContext(),
                "Update", "Packet Liste wird vom Server Geladen", true, false);
        new AsyncTask<Void,Void,List<String>>(){

            @Override
            protected List<String> doInBackground(Void... params) {
                return data.getPackeageList();
            }

            @Override
            protected void onPostExecute( final List<String> result ) {
                dialog.cancel();
                Update_Data.this.updateList(result);
            }
        }.execute();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

}
