package com.googlemail.australianfan.control;

import android.app.Activity;
import android.content.Intent;

import com.googlemail.australianfan.model.ThemColor;
import com.googlemail.australianfan.view.R;

/**
 * Created by xtntx on 06.07.15.
 * Zum wechseln der Farbe einer Activity
 */
public class TheamChooser {

    /**
     * Setzt das Theme und Startet die Acktivity neu
     */
    public static void changeToTheme(Activity activity, ThemColor theme)
    {
        GameInstance.getInstance().setThemColor(theme);
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
    }

    /**
     * Liefert den Int wert des Themse zurück
     * @return R.style Value
     */
    public static int getStyleInt(){
        ThemColor sTheme = GameInstance.getInstance().getThemColor();
        int value;
        if(sTheme == ThemColor.Orange){
            value = R.style.Orange_with_White_Text;
        }else if(sTheme == ThemColor.Black){
            value = R.style.Black_with_White_Text;
        }else if(sTheme == ThemColor.Blau){
            value = R.style.Blue_with_White_Text;
        }else {
            value = R.style.Green_with_Black_Text;
        }

        return value;
    }

    /**
     * Setzt die Style der Activity
     * @param activity die Activity die den Style erhalten soll
     */
    public static void onActivityCreateSetTheme(Activity activity)
    {
        activity.setTheme(getStyleInt());
    }
}
