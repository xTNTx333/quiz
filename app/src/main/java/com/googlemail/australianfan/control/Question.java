package com.googlemail.australianfan.control;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;

/**
 * Created by xtntx on 06.05.15.
 */
public class Question {
    /**
     * Generiert eine Frage
     * @param question Frage von der Datenbank
     */
    public Question(com.googlemail.australianfan.model.Question question){
        this.question = question.getQuestion();
        answers.add(new Answer(question.getAnswer1(), 1 == question.getRightAnswer()));
        answers.add(new Answer(question.getAnswer2(), 2 == question.getRightAnswer()));
        answers.add(new Answer(question.getAnswer3(), 3 == question.getRightAnswer()));
        answers.add(new Answer(question.getAnswer4(), 4 == question.getRightAnswer()));

        if(question.getImage() != null && !question.getImage().isEmpty()){
            hasImage = true;
        }

        dbQuestion = question;
    }

    private com.googlemail.australianfan.model.Question dbQuestion;
    private String question;
    private List<Answer> answers = new ArrayList<Answer>(4);
    private Bitmap image;
    private boolean hasImage;

    /**
     * String der Frage
     * @return
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Liste mit den Antworten
     * @return Die Antworten
     */
    public List<Answer> getAnswers() {
        return answers;
    }

    /**
     * Liefert ein Bild zuruek
     * @return Das Bild
     */
    public Bitmap getImage() {
        try {
            if (image == null && hasImage) {
                String data = dbQuestion.getImage();
                byte[] bytes = Base64.decode(data, Base64.DEFAULT);
                image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            }
        }catch (IllegalFormatException e){
            e.printStackTrace();
        }catch (RuntimeException e){
            e.printStackTrace();
        }

        return image;
    }

    /**
     * Giebt an ob die Frage ein Bild hat
     * @return true wenn Bild vorhanden
     */
    public boolean HasImage() {
        return hasImage;
    }

    public boolean HasRightAnsert(){
        return dbQuestion.isHasAnswerRight();
    }

    public void setHasRightAnsert(boolean isRight){
            dbQuestion.setHasAnswerRight(isRight);
    }

    public com.googlemail.australianfan.model.Question getDbQuestion(){
        return dbQuestion;
    }

    public Integer getID(){
       return dbQuestion.getId();
    }

    /**
     * Klasse die Eine Antwort darstellt.
     */
    public class Answer{
        /**
         * Eine Antwort
         * @param text Text der Antwort
         * @param isRightAnswer True = Antwort ist Koregt
         */
        private Answer(String text, boolean isRightAnswer) {
            Text = text;
            this.isRightAnswer = isRightAnswer;
        }

        /**
         * Text der Antwort
         */
        private String Text;

        /**
         * True wenn antwort korregt ist
         */
        private boolean isRightAnswer;

        /**
         * Antwort Text
         * @return Text der Antwort
         */
        public String getText() {
            return Text;
        }

        /**
         * Ob die Antwort richtig ist
         * @return True wenn richtig
         */
        public boolean isRightAnswer() {
            return isRightAnswer;
        }
    }
}
