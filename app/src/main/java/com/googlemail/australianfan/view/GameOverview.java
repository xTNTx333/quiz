package com.googlemail.australianfan.view;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.InputType;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.googlemail.australianfan.control.DatabaseHelper;
import com.googlemail.australianfan.control.GameInstance;
import com.googlemail.australianfan.control.TheamChooser;
import com.googlemail.australianfan.model.Highscore;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.sql.SQLException;
import java.util.List;


public class GameOverview extends OrmLiteBaseActivity<DatabaseHelper> implements  Answer.OnFragmentInteractionListener{
    private Answer answer1;
    private Answer answer2;
    private Answer answer3;
    private Answer answer4;
    private Question question;
    private Picture picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TheamChooser.onActivityCreateSetTheme(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_overview);
        FragmentManager fragmentManager = getFragmentManager();
        answer1 = (Answer) fragmentManager.findFragmentById(R.id.Button1);
        answer2 = (Answer) fragmentManager.findFragmentById(R.id.Button2);
        answer3 = (Answer) fragmentManager.findFragmentById(R.id.Button3);
        answer4 = (Answer) fragmentManager.findFragmentById(R.id.Button4);
        question = (Question) fragmentManager.findFragmentById(R.id.Question);
        picture = (Picture) fragmentManager.findFragmentById(R.id.Picture);
        UpdateQuestion();
    }

    /**
     * Holt neue Frage und aendert die Ansicht
     */
    private void UpdateQuestion(){
        GameInstance generator = GameInstance.getInstance();
        if(generator.hasNextQuestion()) {
            com.googlemail.australianfan.control.Question questionO = generator.getQuestion();
            question.setQuestion(questionO.getQuestion());
            if (questionO.HasImage()) {
                picture.setImage(questionO.getImage());
            } else {
                picture.setImage(null);
            }

            List<com.googlemail.australianfan.control.Question.Answer> answers =
                    questionO.getAnswers();
            if (answers.size() == 4) {
                answer1.setValues(answers.get(0).getText(), answers.get(0).isRightAnswer());
                answer2.setValues(answers.get(1).getText(), answers.get(1).isRightAnswer());
                answer3.setValues(answers.get(2).getText(), answers.get(2).isRightAnswer());
                answer4.setValues(answers.get(3).getText(), answers.get(3).isRightAnswer());
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game_overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Behandelt die Aktion von Answer
     * @param isAnswerCorect true wenn Richtig geantwortet
     */
    @Override
    public void onFragmentInteraction(boolean isAnswerCorect) {
        GameInstance.getInstance().getQuestion().setHasRightAnsert(isAnswerCorect);
        GameInstance.getInstance().nexQuestion();
        if(isAnswerCorect){
            GameInstance.getInstance().addPoits(1000);
        }

        if(GameInstance.getInstance().hasNextQuestion()){
            UpdateQuestion();
        }else{
            finishQuestion();
        }
    }

    /**
     * Schliest die Activity und Fragt den Namen fuer die Highscore ab;
     */
    private void finishQuestion(){
        GameInstance.getInstance().UpdateQuestions();
        ContextThemeWrapper themeWrapper = new ContextThemeWrapper(this, TheamChooser.getStyleInt());
            AlertDialog.Builder builder = new AlertDialog.Builder(themeWrapper);
            builder.setCancelable(false);
            builder.setTitle("Highscore");

            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setText("Nobody");
            builder.setView(input);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String name = input.getText().toString();
                    if (name.isEmpty()) {
                        name = "Nobody";
                    }

                    try {
                        DatabaseHelper.getInstance()
                                .getHighscorDao().create(
                                new Highscore(GameInstance.getInstance().getPoits(), name));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                    finish();
                }
            });

            builder.show();
    }

    @Override
    public void onBackPressed() {
    }
}
