package com.googlemail.australianfan.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.googlemail.australianfan.control.DatabaseHelper;
import com.googlemail.australianfan.control.GameInstance;
import com.googlemail.australianfan.control.TheamChooser;
import com.googlemail.australianfan.model.ThemColor;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;


public class StartSite extends OrmLiteBaseActivity<DatabaseHelper> implements View.OnClickListener{
    private ThemColor color;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TheamChooser.onActivityCreateSetTheme(this);
        color = GameInstance.getInstance().getThemColor();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_site);
        Button button1 = (Button) findViewById(R.id.start_button);
        Button button2 = (Button) findViewById(R.id.settings_button);
        Button bHighscore = (Button) findViewById(R.id.bHighscore);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        bHighscore.setOnClickListener(this);
    }

    @Override
    protected  void onStart(){
        if(color != GameInstance.getInstance().getThemColor()){
            TheamChooser.changeToTheme(this, GameInstance.getInstance().getThemColor());
            return;
        }

        super.onStart();
    }

    /**
     * Behandelt die buttons clicks
     * @param arg0 Die view
     */
    @Override
    public void onClick(View arg0) {
        Intent i;
        switch (arg0.getId()) {
            case (R.id.start_button):
                i = new Intent(this, GameOverview.class);
                startActivity(i);

                break;

            case (R.id.settings_button):
                i = new Intent(this, Settings.class);
                startActivity(i);

                break;

            case  (R.id.bHighscore):
                i = new Intent(this, Highscore.class);
                startActivity(i);
        }
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start_site, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
