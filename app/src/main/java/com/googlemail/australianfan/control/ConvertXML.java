package com.googlemail.australianfan.control;

import android.util.Log;

import com.googlemail.australianfan.model.Question;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

/**
 * Created by xtntx on 30.06.15.
 * Parst die XML File
 */
public class ConvertXML {
    /**
     * Factory des XML Pasers
     */
    private XmlPullParserFactory xmlFactoryObject = null;

    /**
     * Der XML Parser
     */
    private XmlPullParser myparser = null;

    /**
     * Läde den Parser und die Factory
     * @throws XmlPullParserException
     */
    public ConvertXML() throws XmlPullParserException {
        xmlFactoryObject = XmlPullParserFactory.newInstance();
        myparser = xmlFactoryObject.newPullParser();
    }

    /**
     * Parst das XML File und Updatet die DB
     * @param file Das Xml File
     * @param packagename Der Name des Packets
     * @return True wenn alles geklapt hat
     */
    public boolean convertXML(File file, String packagename){
        try {
            return convertXML(new FileInputStream(file), packagename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Parst das XML File und Updatet die DB
     * @param file Das Xml File
     * @param packagename Der Name des Packets
     * @return True wenn alles geklapt hat
     */
    public boolean convertXML(InputStream file, String packagename){
        try {
            myparser.setInput(file, null);
            int event = myparser.getEventType();
            com.googlemail.australianfan.model.Question question = null;
            String currentTag = null;
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myparser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        if(name.equals("questiondata")){
                            question = new Question();
                        }
                        currentTag = myparser.getName();
                        break;
                    case XmlPullParser.IGNORABLE_WHITESPACE:
                        break;
                    case XmlPullParser.TEXT:
                        String test = myparser.getName();
                        if (currentTag.equals("question")) {
                            question.setQuestion(myparser.getText());
                        }else if(currentTag.equals("answer1")){
                            question.setAnswer1(myparser.getText());
                        }else if(currentTag.equals("answer2")){
                            question.setAnswer2(myparser.getText());
                        }else if(currentTag.equals("answer3")){
                            question.setAnswer3(myparser.getText());
                        }else if(currentTag.equals("answer4")){
                            question.setAnswer4(myparser.getText());
                        }else if(currentTag.equals("rightAnswer")){
                            String text = myparser.getText();
                            if(text.matches("[[1-4]]")) {
                                question.setRightAnswer(Integer.parseInt(text));
                            }
                        }else if(currentTag.equals("image")) {
                            question.setImage(myparser.getText());
                        }
                        break;

                    case XmlPullParser.END_TAG:
                        currentTag = "";
                        if(name.equals("questiondata")){
                            question.setPackagename(packagename);
                            Log.d(this.getClass().getName(), question.toString());
                            DatabaseHelper.getInstance().getQuestionsDao().create(question);
                        }

                        break;
                }
                event = myparser.next();
            }
            return true;
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
