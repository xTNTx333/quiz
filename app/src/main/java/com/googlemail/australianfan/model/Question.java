package com.googlemail.australianfan.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by xtntx on 05.05.15.
 * Klasse stellt die Fragen dar die Gestellt werden k�nnen.
 */

@DatabaseTable(tableName = "QUESTION")
public class Question implements Serializable {
    /**
     * Konstruktor fuer den Ormapper
     */
    public Question() {
    }

    /**
     * Schnelles befullen des Frage
     * @param question Die Frage
     * @param answer1 Text der ersten Antwort
     * @param answer2 Text der zweiten Antwort
     * @param answer3 Text der dritten Antwort
     * @param answer4 Text der vierten Antwort
     * @param rightAnswer Nummer der Richtigen Antwort
     * @param image Bild Base64 Codiert
     */
    public Question(String question, String answer1, String answer2, String answer3, String answer4, Integer rightAnswer, String image) {
        this.question = question;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.rightAnswer = rightAnswer;
        this.image = image;
    }

    //Block um abfragen leichter erstellen zu konnen
    public final static String ID = "ID";
    public final static String QUESTION = "QUESTION";
    public final static String ANSWER_1 = "ANSWER_1";
    public final static String ANSWER_2 = "ANSWER_2";
    public final static String ANSWER_3 = "ANSWER_3";
    public final static String ANSWER_4 = "ANSWER_4";
    public final static String RIGHT_ANSWER = "RIGHT_ANSWER";
    public final static String IMAGE = "IMAGE";
    public final static String HAS_ANSWER_RIGHT = "HAS_ANSWER_RIGHT";
    public final static String PACKAGE_NAME = "PACKAGE_NAME";

    @DatabaseField(allowGeneratedIdInsert=true, generatedId = true, columnName = ID)
    private int Id;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = QUESTION)
    private String question;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = ANSWER_1)
    private String answer1;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = ANSWER_2)
    private String answer2;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = ANSWER_3)
    private String answer3;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = ANSWER_4)
    private String answer4;

    @DatabaseField(canBeNull = false, dataType = DataType.INTEGER, columnName = RIGHT_ANSWER)
    private int rightAnswer;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = IMAGE, defaultValue = "")
    private String image;

    @DatabaseField(canBeNull = false, dataType = DataType.BOOLEAN, defaultValue = "false", columnName = HAS_ANSWER_RIGHT)
    private boolean hasAnswerRight = false;

    @DatabaseField(canBeNull = true, dataType = DataType.STRING, columnName = PACKAGE_NAME, defaultValue = "Standart")
    private String packagename;

    /**
     * Eindeutige Id der Frage
     * @return Integer
     */
    public int getId() {
        return Id;
    }

    /**
     * Setzt die Id vorsicht walten lassen da Fehler bei es sich um einen Key handelt
     * @param id Der Neue wert
     */
    public void setId(int id) {
        Id = id;
    }

    /**
     * Die Fragestellung
     * @return String mit der Frage
     */
    public String getQuestion() {
        return question !=null ? question : "";
    }

    /**
     * Setzt eine Neue Fragestellung
     * @param question String mit neuer Frage
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     * Liefert die erste antwort zur�ck
     * @return Die erste Antwort
     */
    public String getAnswer1() {
        return answer1;
    }

    /**
     * Setzt den Wert der ersten Antwort
     * @param answer1 Text der ersten Antwort
     */
    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    /**
     * Liefert die zweite Antwort zur�ck
     * @return Die zweite Antwort
     */
    public String getAnswer2() {
        return answer2;
    }

    /**
     * Setzt den Wert der zweiten Antwort
     * @param answer2 Text der zweiten Antwort
     */
    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    /**
     * Liefert die dritte antwort zur�ck
     * @return Die dritte Antwort
     */
    public String getAnswer3() {
        return answer3;
    }

    /**
     * Setzt den Wert der dritten Antwort
     * @param answer3 Text der dritten Antwort
     */
    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    /**
     * Liefert die vierte antwort zur�ck
     * @return Die vierte Antwort
     */
    public String getAnswer4() {
        return answer4;
    }

    /**
     * Setzt den Wert der vierten Antwort
     * @param answer4 Text der vierten Antwort
     */
    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    /**
     * Liefert die richtige Nummer der Antwort zurück
     * @return Nummer der richtigen Antwort
     */
    public int getRightAnswer() {
        return rightAnswer;
    }

    /**
     * Setzt die Richtige antwort
     * @param rightAnswer Nummer der Richtigen Antwort
     */
    public void setRightAnswer(int rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    /**
     * Image zur Frage
     * @return Bild im Base64 Format
     */
    public String getImage() {
        return image;
    }

    /**
     * Speichert Bild in der Datenbank
     * @param image Bild Base64 Codiert
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Giebt an ob die Antwort richtig beantwortet worden ist
     * @return true wenn die Antwort richtig beantwortet wurde
     */
    public boolean isHasAnswerRight() {
        return hasAnswerRight;
    }

    /**
     * Zu setzen wenn die Antwort richtig beantwortet war
     * @param hasAnswerRight True wenn Antwort richtig
     */
    public void setHasAnswerRight(boolean hasAnswerRight) {
        this.hasAnswerRight = hasAnswerRight;
    }

    /**
     * Liefert den Packedname worüber die Frage hinzugefügt worden ist.
     * @return Den Packet name
     */
    public String getPackagename() {
        return packagename;
    }

    /**
     * Setzt den Packet name
     * @param packagename der Packet name
     */
    public void setPackagename(String packagename) {
        this.packagename = packagename;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Id =").append(Id).append("\n").append(question).append("\n").append(answer1)
                .append("\n").append(answer2).append("\n").append(answer3).append("\n")
                .append(answer4).append("\nPackage: ").append(packagename);
        return sb.toString();
    }
}
