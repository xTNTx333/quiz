package com.googlemail.australianfan.control;

import com.googlemail.australianfan.model.Highscore;
import com.googlemail.australianfan.model.Settings;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;

/**
 * Created by xtntx on 05.05.15.
 */
public class UpdateORmapper extends OrmLiteConfigUtil {

    /**
     * Klassen die gemappt werden
     */
    private static final Class<?>[] classes = new Class[] {
            Highscore.class,
            com.googlemail.australianfan.model.Question.class,
            Settings.class
    };

    /**
     * Beim Ausfueren des Programm wird die ormlite_config erzeugt.
     * @param args nichts zu tun
     * @throws Exception IO Exzeption
     */
    public static void main(String[] args) throws Exception {
        String userHome = System.getProperty( "user.home" );
        File configFile = new File(userHome + "/AndroidStudioProjects/Quiz/app/src/main/res" +
                "/raw/ormlite_config.txt");
        if(configFile.exists()){
            configFile.delete();
        }
        writeConfigFile(configFile, classes);
    }
}
