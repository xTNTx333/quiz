package com.googlemail.australianfan.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.googlemail.australianfan.control.TheamChooser;
import com.googlemail.australianfan.model.ThemColor;

import java.util.LinkedList;
import java.util.List;


public class Settings extends Activity implements Update_Data.OnFragmentInteractionListener {

    private List<ThemColor> themes = new LinkedList<ThemColor>();
    private ArrayAdapter<ThemColor> themeArrayAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TheamChooser.onActivityCreateSetTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        for(ThemColor color : ThemColor.values()){
            themes.add(color);
        }

        themeArrayAdapter = new ArrayAdapter<ThemColor>(getBaseContext(), android.R.layout.simple_list_item_1, themes);
        listView = (ListView) findViewById(R.id.farbe);
        listView.setAdapter(themeArrayAdapter);
        final Activity activity = this;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ThemColor color = (ThemColor) parent.getItemAtPosition(position);
                if(color != null){
                    TheamChooser.changeToTheme(activity, color);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
