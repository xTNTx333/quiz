package com.googlemail.australianfan.view;

import android.app.Application;
import android.content.Context;

import com.googlemail.australianfan.control.DatabaseHelper;

/**
 * Created by xtntx on 30.06.15.
 * Klasse die den Globalen Application context enthält
 */
public class ApplicationContextProvider extends Application {

    private static Context context;

    @Override
    public void onCreate(){
        super.onCreate();
        context = getApplicationContext();
        new DatabaseHelper(context);
    }

    /**
     * Giebt den AplicationContext zurück
     * @return AplicationContext
     */
    public static Context getAplicationContext(){
        return context;
    }
}
