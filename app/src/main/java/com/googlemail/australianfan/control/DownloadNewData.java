package com.googlemail.australianfan.control;

import android.content.Context;
import android.util.Log;

import com.googlemail.australianfan.model.Question;
import com.googlemail.australianfan.view.ApplicationContextProvider;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by xtntx on 27.06.15.
 * Klasse zum Downloaden und suchen nach XML Datein
 */
public class DownloadNewData {
    /**
     * Adresse des Servers mit den sich verbunden werden soll
     */
    private final String ip = "xtntx.bplaced.net";

    /**
     * Loginname des Users
     */
    private final String userName = "xtntx_test";

    /**
     * Passwort des Users
     */
    private final String pass = "test";

    /**
     * Objekt das die Kommunikation mit dem FTP Server ermöglicht
     */
    private FTPClient mFtpClient;

    /**
     * Basis Pfad auf dem Server
     */
    private String basePath;

    /**
     * Giebt an ob der Client sich mit den Server verbunden hat
     */
    boolean status = false;

    /**
     * Aufbau der Verbindung zum Server
     */
    private void conectToFTP() {
        try {
            mFtpClient = new FTPClient();
            mFtpClient.setConnectTimeout(3 * 1000);
            InetAddress adresse = InetAddress.getByName(ip);
            mFtpClient.connect(adresse);
            status = mFtpClient.login(userName, pass);
            Log.e("isFTPConnected", String.valueOf(status));
            if (FTPReply.isPositiveCompletion(mFtpClient.getReplyCode())) {
                mFtpClient.setFileType(FTP.ASCII_FILE_TYPE);
                mFtpClient.enterLocalPassiveMode();
                status = true;
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Die Verbindung zum Schließen des Servers
     */
    private void Disconect(){
        try {
            mFtpClient.logout();
            mFtpClient.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            status = false;
        }
    }

    /**
     * Liefert die vorhandenen Packete auf dem Server
     * @return Liste mit den Namen
     */
    public List<String> getPackeageList(){
        List<String> packeages = new LinkedList<>();
        try {
            List<String> installedPackages = getInstaletPackeages();
            conectToFTP();
            FTPFile[] mFileArray = mFtpClient.listFiles();
            for(FTPFile file : mFileArray){
                if(file.isFile()){
                    String fileName = file.getName();
                    if(fileName.endsWith(".xml") && !installedPackages.contains(fileName)){
                        packeages.add(fileName.replace(".xml",""));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            Disconect();
        }

        return  packeages;
    }

    /**
     * Liefert die Instalierten Packete zurück
     * @return Liste mit den Instalierten Packete
     * @throws SQLException
     */
    private List<String> getInstaletPackeages() throws SQLException {
        RuntimeExceptionDao<Question,Integer> questions =
                DatabaseHelper.getInstance().getQuestionsRuntimeDao();
        QueryBuilder queryBuilder = questions.queryBuilder();
        //queryBuilder.distinct().selectColumns(Question.PACKAGE_NAME);
        Where where = queryBuilder.where();
        where.isNotNull(Question.PACKAGE_NAME);
        queryBuilder.setWhere(where);
        List<Question> questionList;
        try {
            questionList = queryBuilder.query();
        }catch (NullPointerException e){
            questionList = new LinkedList<Question>();
        }

        List<String> installedPackages = new LinkedList<String>();
        for(Question question : questionList){
            String name = question.getPackagename();
            if(name != null) {
                installedPackages.add(name +".xml");
            }
        }
        return installedPackages;
    }

    /**
     * Läde das Packege Runter und Instaliert dieses
     * @param sPackage Name des Packet
     * @return True wenn ales geklapt hat
     */
    public boolean downloadPackage(String sPackage){
        try {
        conectToFTP();
        if(status == true) {
            String workingDir = mFtpClient.printWorkingDirectory();
            String srcFilePath = workingDir + sPackage + ".xml";
            Context context = ApplicationContextProvider.getAplicationContext();
            File path = context.getCacheDir();
            File desFile = new File(path, "temp.xml");//File.createTempFile("Quiz", ".xml", ApplicationContextProvider.getAplicationContext().getCacheDir());
            desFile.deleteOnExit();
            FileOutputStream desFileStream = new FileOutputStream(desFile);
            status = mFtpClient.retrieveFile(srcFilePath, desFileStream);
            desFileStream.close();
            ConvertXML converter = new ConvertXML();
            return converter.convertXML(desFile, sPackage);
        }
        }catch (IOException ex) {
            ex.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } finally {
            Disconect();
        }
        return false;
    }
}
