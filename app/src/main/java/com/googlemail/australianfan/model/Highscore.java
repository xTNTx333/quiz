package com.googlemail.australianfan.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by xtntx on 05.05.15.
 * Klasse die Einzelne Spalten Darstellt in der Highscore
 */
@DatabaseTable(tableName = "HIGHSCORE")
public class Highscore implements Serializable {
    /**
     * Konstruktor fuer den Ormapper
     */
    public Highscore() {
    }

    /**
     * Konstruktor fuer Schnellen bef�llen
     * @param points Punkte des Spielers
     * @param name Name des Spielers
     */
    public Highscore(Integer points, String name) {
        this.points = points;
        this.name = name;
    }

    public static final String NAME = "NAME";
    public static final String POINTS = "POINTS";

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = NAME)
    private String name;

    @DatabaseField(columnName = POINTS)
    private int points;

    /**
     * Liefert den Namen zurueck
     * @return Name des Spielers
     */
    public String getName() {
        return name;
    }

    /**
     * Setzt den Namen des Spielers
     * @param name Name des Spielers
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Punkte des Spielers
     * @return Punkte des Spielers
     */
    public int getPoints() {
        return points;
    }

    /**
     * Setzt die Punkte des Spielers
     * @param points Punkte des Spielers
     */
    public void setPoints(int points) {
        this.points = points;
    }

    public String toString(){
        return  name + " Punkte: " + points;
    }
}
