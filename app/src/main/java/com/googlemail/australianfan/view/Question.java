package com.googlemail.australianfan.view;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Question#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Question extends Fragment{
    private static final String ARG_Question = "question";
    private String question;
    private TextView textView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param question Parameter 1.
     * @return A new instance of fragment Question.
     */
    public static Question newInstance(String question) {
        Question fragment = new Question();
        Bundle args = new Bundle();
        args.putString(ARG_Question, question);
        fragment.setArguments(args);
        return fragment;
    }

    public Question() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            question = getArguments().getString(ARG_Question);
        }
    }

    /**
     * Setzt die Frage
     * @param question Die Frage
     */
    public void setQuestion(String question){
        this.question = question;
        textView.setText(question);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_question, container, false);
        textView = (TextView) view.findViewById(R.id.textQuestion);
        return view;
    }


}
