package com.googlemail.australianfan.view;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Answer.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Answer#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Answer extends Fragment {
    private static final String ANSWER = "answer";
    private static final String IS_RIGHT = "isRight";
    private View view;
    private android.widget.Button button;
    private String answer;
    private String isRight;
    private static boolean handelAktion = true;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param answer Die Antwort
     * @param isRight True wenn richtig
     * @return A new instance of fragment Button.
     */
    public static Answer newInstance(String answer, String isRight) {
        Answer fragment = new Answer();
        Bundle args = new Bundle();
        args.putString(ANSWER, answer);
        args.putString(IS_RIGHT, isRight);
        fragment.setArguments(args);
        return fragment;
    }

    public Answer() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            answer = getArguments().getString(ANSWER);
            isRight = getArguments().getString(IS_RIGHT);
        }
    }

    /**
     * Setzt die die Antworten
     * @param answer die Antwort
     * @param isRight true wenn die Antwort Korect ist
     */
    public void setValues(String answer, boolean isRight){
        this.answer = answer;
        this.isRight = String.valueOf(isRight);
        button.setText(answer);
        view.invalidate();
    }

    /**
     * Setzt die antwort in Gruen oder Rot
     * @param view die View auf der die Antwort aktiv ist
     */
    private void setAnswerColor(final View view){
        int color;
        final boolean isAnswerCorect;
        if(isRight.compareToIgnoreCase("true") == 0){
            color = Color.parseColor("#00FF00");
            isAnswerCorect = true;
        }else{
            color = Color.parseColor("#FF0000");
            isAnswerCorect = false;
        }

        button.setBackgroundColor(color);
        view.invalidate();

        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try{
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void params) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void params) {
                        button.setBackgroundColor(getResources().getColor(R.color.buttonDefaultColor));
                        handelAktion = true;
                        onButtonPressed(isAnswerCorect);
                    }
                }.execute();
            }

        }.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_button, container, false);
        this.view = view;
        button = (android.widget.Button) view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (handelAktion) {
                    handelAktion = false;
                    button.setBackgroundColor(Color.parseColor("#FFCC00"));
                    v.invalidate();
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void params) {
                            setAnswerColor(view);
                        }
                    }.execute();
                }
            }
        });

        return view;
    }

    public void onButtonPressed(boolean isAnswerCorect) {
        if (mListener != null) {
            mListener.onFragmentInteraction(isAnswerCorect);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(boolean isAnswerCorect);
    }

}
