package com.googlemail.australianfan.control;

import android.util.Log;

import com.googlemail.australianfan.model.Settings;
import com.googlemail.australianfan.model.ThemColor;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xtntx on 07.05.15.
 * Klasse zum generieren und updaten von FrageListen
 */
public class GameInstance {
    private final int questionSize;
    private Map<Integer, Question> asketQuestion = new HashMap<Integer, Question>();
    private int number;
    private int poits;
    private Settings setting;
    private static final int MAX_QUESTION = 10;

    private static GameInstance gameInstance;
    /**
     * Konstructor mit der Frage wie viele Fragen gestelt werden sollen
     * @param questionSize Anzahl der Fragen
     */
    private GameInstance(int questionSize){
        this.questionSize = questionSize;
        try {
            Dao<Settings, Integer> dao = DatabaseHelper.getInstance().getSettingsDao();
            List<Settings> settings = dao.queryForAll();
            if(settings.size() > 0) {
                setting = settings.get(0);
            }else{
                setting = new Settings();
                setting.setColor(ThemColor.Orange);
                setting.setName("NoBody");
                dao.create(setting);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        if(setting == null){
            setting = new Settings();
            setting.setColor(ThemColor.Orange);
            setting.setName("NoBody");
            Dao<Settings, Integer> dao = null;
            try {
                dao = DatabaseHelper.getInstance().getSettingsDao();
                dao.create(setting);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Liefert die Instance der Klasse zuruek als Singelton ausgelegt da sontst beim Rotieren Fehler
     * enstehen
     * @return Die Instance
     */
    public static GameInstance getInstance(){
        if(gameInstance == null){
            gameInstance = new GameInstance(MAX_QUESTION);
        }

        return gameInstance;
    }

    /**
     * Liefert eine Liste an Fragen zuruck und merkt sich die zum spateren Update
     * @return Liste mit Fragen
     * @throws SQLException Wenn mit der Anfrage mit der Datenbank was schief geht
     */
    private void getQuestions() throws SQLException {
        RuntimeExceptionDao<com.googlemail.australianfan.model.Question, Integer> dao
                = DatabaseHelper.getInstance().getQuestionsRuntimeDao();
        float count = dao.countOf();
        if(count < questionSize){
            Log.e(this.getClass().getName(), "Liste der Fragen kleiner als angefordert");
            throw new RuntimeException("Liste der Fragen kleiner als angeforderten Fragen");
        }

        List<com.googlemail.australianfan.model.Question> questionsForSelct =
                dao.queryForEq("HAS_ANSWER_RIGHT", false);
        int putQuestion = questionSize;
        if(questionsForSelct.size() > questionSize){
            for(com.googlemail.australianfan.model.Question question : questionsForSelct){
                if(!asketQuestion.containsKey(question.getId())){
                    asketQuestion.put(question.getId(), new Question(question));
                    putQuestion--;
                }
            }
        }

        for(int i = 0; i < putQuestion; ++i){
            int questionNumber = (int)((Math.random() * count * 10) % count);
            com.googlemail.australianfan.model.Question question = dao.queryForId(questionNumber);
            if(question != null && !asketQuestion.containsKey(question.getId())) {
                asketQuestion.put(question.getId(), new Question(question));
            }else{
                i -= 1;
            }
        }
    }

    /**
     * Prueft ob noch eine Frage vorhanden ist
     * @return True wenn Frage vorhanden ist
     */
    public boolean hasNextQuestion(){
        return number < MAX_QUESTION;
    }

    /**
     * Setzt den internen Zaehler weiter
     */
    public void nexQuestion(){
        number += 1;
    }

    /**
     * Fuegt weitere Punkte zum Punktestand hinzu
     * @param poits Punkte
     */
    public void addPoits(int poits){
        this.poits += poits;
    }

    /**
     * Liefert die Punkte zuruek
     * @return Punkte
     */
    public int getPoits(){
        return poits;
    }

    public ThemColor getThemColor(){
        return setting.getColor();
    }

    public void setThemColor(ThemColor color){
        setting.setColor(color);
        try {
            DatabaseHelper.getInstance().getSettingsDao().update(setting);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getName(){
        return setting.getName();
    }

    public void setName(String name){
        setting.setName(name);
        try {
            DatabaseHelper.getInstance().getSettingsDao().update(setting);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Liefert die aktuelle Frage zuruek
     * @return Frage
     */
    public Question getQuestion(){
        if(asketQuestion.isEmpty()){
            try {
                getQuestions();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }

        return asketQuestion.get(asketQuestion.keySet().toArray()[number]);
    }

    /**
     * Updatet die Fragen und setzt Game Instance zum anfangszustand zuruek.
     */
    public void UpdateQuestions(){
        Log.d(this.getName(), "Update Question");
        try {
            RuntimeExceptionDao<com.googlemail.australianfan.model.Question, Integer> dao
                    = DatabaseHelper.getInstance().getQuestionsRuntimeDao();
            for (com.googlemail.australianfan.control.Question question : asketQuestion.values()) {
                UpdateBuilder<com.googlemail.australianfan.model.Question, Integer> updateBuilder =
                        dao.updateBuilder();
                boolean temp = question.HasRightAnsert();
                updateBuilder.updateColumnValue(com.googlemail.australianfan.model.Question.HAS_ANSWER_RIGHT, temp);
                updateBuilder.where().eq(com.googlemail.australianfan.model.Question.ID, question.getID());
                Log.d(this.getName(), String.valueOf(updateBuilder.update()));
                //dao.update(question.getDbQuestion());
            }


        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(this.getName(), e.getMessage());
        }

        asketQuestion.clear();
        number = 0;
    }
}
