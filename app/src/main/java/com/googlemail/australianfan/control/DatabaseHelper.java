package com.googlemail.australianfan.control;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.googlemail.australianfan.model.DatabaseValues;
import com.googlemail.australianfan.model.Highscore;
import com.googlemail.australianfan.model.Settings;
import com.googlemail.australianfan.view.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by xtntx on 06.05.15.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    /*
     * Name der Datenbank
     */
    private static final String DATABASE_NAME = "Quiz.db";

    /*
     *Version der Datenbank, bei Änderrungen muss die nummer rauf gesetzt werden
     */
    private static final int DATABASE_VERSION = 8;
    /*
     * Instance um auch von der control aus den DataHelper zu erreichen.
     */
    private static DatabaseHelper instance;
    /**
     * Log tag zum loggen der Nachrichten
     */
    private final String logTag = this.getClass().getName();
    /*
     * Dao Objekt zum Mappen der Tabelle Question
     */
    private Dao<com.googlemail.australianfan.model.Question, Integer> questionsDao = null;
    /*
     * Dao Objekt zum Mappen der Tabelle Question und wirft Laufzeitfehler
     */
    private RuntimeExceptionDao<com.googlemail.australianfan.model.Question, Integer> questionsRuntimeDao = null;
    /*
     *DAO Objekt zum Mappen der Tabelle Highscore
     */
    private Dao<Highscore, Integer> highscorDao = null;
    /*
     *DAO Objekt zum Mappen der Tabelle Highscore und wirft Laufzeitfehler
     */
    private RuntimeExceptionDao<Highscore, Integer> highscorRuntimeDao = null;
    /*
     *DAO Objekt zum Mappen der Tabelle Settings
     */
    private Dao<Settings, Integer> settingsDao = null;
    /*
     *DAO Objekt zum Mappen der Tabelle Settings und wirft Laufzeitfehler
     */
    private RuntimeExceptionDao<Settings, Integer> settingsRuntimeDao = null;

    /**
     * Konstruktor der die ormlite_config ausliest.
     * @param context Kontext der App
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
        instance = this;
    }

    public static DatabaseHelper getInstance() {
        return instance;
    }

    /**
     * What to do when your database needs to be created. Usually this entails creating the tables and loading any
     * initial data.
     * <p/>
     * <p>
     * <b>NOTE:</b> You should use the connectionSource argument that is passed into this method call or the one
     * returned by getConnectionSource(). If you use your own, a recursive call or other unexpected results may result.
     * </p>
     *
     * @param database         Database being created.
     * @param connectionSource Verbindungsstring zur Datenbank
     */
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.i(logTag, "onCreate");
            TableUtils.createTable(connectionSource, Highscore.class);
            TableUtils.createTable(connectionSource, com.googlemail.australianfan.model.Question.class);
            TableUtils.createTable(connectionSource, Settings.class);
        } catch (SQLException e) {
            Log.e(logTag, "Can't create database", e);
            throw new RuntimeException(e);
        }

        try {
            DatabaseValues.getQuestions(getQuestionsRuntimeDao());
            DatabaseValues.getHighscores(getHighscorRuntimeDao());
        } catch (SQLException e) {
            Log.e(logTag, "Befüllen der Datenbank beim Update gescheitert", e);
        }
    }

    /**
     * What to do when your database needs to be updated. This could mean careful migration of old data to new data.
     * Maybe adding or deleting database columns, etc..
     * <p/>
     * <p>
     * <b>NOTE:</b> You should use the connectionSource argument that is passed into this method call or the one
     * returned by getConnectionSource(). If you use your own, a recursive call or other unexpected results may result.
     * </p>
     *
     * @param database         Database being upgraded.
     * @param connectionSource To use get connections to the database to be updated.
     * @param oldVersion       The version of the current database so we can know what to do to the database.
     * @param newVersion        neue Datenbankversion
     */
    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(logTag, "onUpgrade");
            TableUtils.dropTable(connectionSource, Highscore.class, true);
            TableUtils.dropTable(connectionSource, com.googlemail.australianfan.model.Question.class, true);
            TableUtils.dropTable(connectionSource, Settings.class, true);
        } catch (SQLException e) {
            Log.e(logTag, "Can't drop databases", e);
            throw new RuntimeException(e);
        }

        questionsDao = null;
        questionsRuntimeDao = null;
        highscorDao = null;
        highscorRuntimeDao = null;
        settingsDao = null;
        settingsRuntimeDao = null;

        onCreate(database, connectionSource);
    }

    /**
     * Gibt ein Data Acres Objekt auf dem die Highscore zuruek
     * @return Dao Highscore
     * @throws SQLException wenn das erstellen des Dao schief geht.
     */
    public Dao<Highscore,Integer> getHighscorDao() throws SQLException{
        if(highscorDao == null){
            highscorDao = getDao(Highscore.class);
        }

        return  highscorDao;
    }

    /**
     * Gibt ein Data Acres Objekt auf dem die Highscore zuruek
     * @return Dao Highscore
     * @throws SQLException wenn das erstellen des Dao schief geht.
     */
    public RuntimeExceptionDao<Highscore, Integer> getHighscorRuntimeDao() throws SQLException{
        if(highscorRuntimeDao == null){
            highscorRuntimeDao = getRuntimeExceptionDao(Highscore.class);
        }

        return highscorRuntimeDao;
    }

    /**
     * Gibt ein Data Acres Objekt auf dem die Question zuruek
     * @return Dao Question
     * @throws SQLException wenn das erstellen des Dao schief geht.
     */
    public Dao<com.googlemail.australianfan.model.Question, Integer> getQuestionsDao() throws SQLException{
        if(questionsDao == null){
            questionsDao = getDao(com.googlemail.australianfan.model.Question.class);
        }

        return questionsDao;
    }

    /**
     * Gibt ein Data Acres Objekt auf dem die Question zuruek
     * @return Dao Question
     * @throws SQLException wenn das erstellen des Dao schief geht.
     */
    public RuntimeExceptionDao<com.googlemail.australianfan.model.Question, Integer> getQuestionsRuntimeDao() throws  SQLException{
        if(questionsRuntimeDao == null){
            questionsRuntimeDao = getRuntimeExceptionDao(com.googlemail.australianfan.model.Question.class);
        }

        return questionsRuntimeDao;
    }

    /**
     * Gibt ein Data Acres Objekt auf dem die Setting zuruek
     * @return Dao Question
     * @throws SQLException wenn das erstellen des Dao schief geht.
     */
    public Dao<Settings, Integer> getSettingsDao() throws SQLException{
        if(settingsDao == null){
            settingsDao = getDao(Settings.class);
        }

        return settingsDao;
    }

    /**
     * Gibt ein Data Acres Objekt auf dem die Setting zuruek
     * @return Dao Question
     * @throws SQLException wenn das erstellen des Dao schief geht.
     */
    public RuntimeExceptionDao<Settings, Integer> getSettingsRuntimeDao() throws  SQLException{
        if(settingsRuntimeDao == null){
            settingsRuntimeDao = getRuntimeExceptionDao(Settings.class);
        }

        return settingsRuntimeDao;
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
        questionsDao = null;
        questionsRuntimeDao = null;
        highscorDao = null;
        highscorRuntimeDao = null;
        settingsDao = null;
        settingsRuntimeDao = null;
    }
}
